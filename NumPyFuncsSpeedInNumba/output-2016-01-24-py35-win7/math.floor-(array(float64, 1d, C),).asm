	.text
	.file	"f"
	.globl	"<dynamic>.f$6.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"<dynamic>.f$6.array(float64,_1d,_C)",@function
"<dynamic>.f$6.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%rsi
.Ltmp0:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
	movaps	%xmm6, 32(%rsp)
.Ltmp1:
	.cfi_def_cfa_offset 64
.Ltmp2:
	.cfi_offset %rsi, -16
.Ltmp3:
	.cfi_offset %xmm6, -32
	movq	%rcx, %rsi
	movq	104(%rsp), %rax
	testq	%r9, %r9
	je	.LBB0_2
	lock
	incq	(%r9)
.LBB0_2:
	xorps	%xmm6, %xmm6
	testq	%rax, %rax
	jle	.LBB0_5
	movq	120(%rsp), %rcx
	incq	%rax
	xorps	%xmm6, %xmm6
	.align	16, 0x90
.LBB0_4:
	movsd	(%rcx), %xmm0
	roundsd	$1, %xmm0, %xmm0
	cvttsd2si	%xmm0, %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm6
	decq	%rax
	addq	$8, %rcx
	cmpq	$1, %rax
	jg	.LBB0_4
.LBB0_5:
	testq	%r9, %r9
	je	.LBB0_8
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r9)
	cmpq	$1, %rax
	je	.LBB0_7
.LBB0_8:
	movsd	%xmm6, (%rsi)
	xorl	%eax, %eax
	movaps	32(%rsp), %xmm6
	addq	$48, %rsp
	popq	%rsi
	retq
.LBB0_7:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r9, %rcx
	callq	*%rax
	jmp	.LBB0_8
.Ltmp4:
	.size	"<dynamic>.f$6.array(float64,_1d,_C)", .Ltmp4-"<dynamic>.f$6.array(float64,_1d,_C)"
	.cfi_endproc

	.globl	"wrapper.<dynamic>.f$6.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"wrapper.<dynamic>.f$6.array(float64,_1d,_C)",@function
"wrapper.<dynamic>.f$6.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%rsi
.Ltmp5:
	.cfi_def_cfa_offset 16
	pushq	%rdi
.Ltmp6:
	.cfi_def_cfa_offset 24
	subq	$136, %rsp
	movaps	%xmm6, 112(%rsp)
.Ltmp7:
	.cfi_def_cfa_offset 160
.Ltmp8:
	.cfi_offset %rdi, -24
.Ltmp9:
	.cfi_offset %rsi, -16
.Ltmp10:
	.cfi_offset %xmm6, -48
	movq	%rcx, %rsi
	leaq	104(%rsp), %rax
	movq	%rax, 32(%rsp)
	movabsq	$.const.f, %rax
	movabsq	$PyArg_UnpackTuple, %rdi
	movl	$1, %r8d
	movl	$1, %r9d
	movq	%rdx, %rcx
	movq	%rax, %rdx
	callq	*%rdi
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	$0, 96(%rsp)
	testl	%eax, %eax
	je	.LBB1_1
	testq	%rsi, %rsi
	je	.LBB1_18
	cmpq	$0, 24(%rsi)
	je	.LBB1_5
	movq	104(%rsp), %rcx
	movabsq	$NRT_adapt_ndarray_from_python, %rax
	leaq	48(%rsp), %rdx
	callq	*%rax
	testl	%eax, %eax
	jne	.LBB1_1
	movq	48(%rsp), %rsi
	movq	64(%rsp), %rax
	movq	80(%rsp), %rcx
	testq	%rsi, %rsi
	je	.LBB1_9
	lock
	incq	(%rsi)
.LBB1_9:
	xorps	%xmm6, %xmm6
	testq	%rax, %rax
	jle	.LBB1_12
	incq	%rax
	xorps	%xmm6, %xmm6
	.align	16, 0x90
.LBB1_11:
	movsd	(%rcx), %xmm0
	roundsd	$1, %xmm0, %xmm0
	cvttsd2si	%xmm0, %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm6
	decq	%rax
	addq	$8, %rcx
	cmpq	$1, %rax
	jg	.LBB1_11
.LBB1_12:
	testq	%rsi, %rsi
	je	.LBB1_17
	movq	$-1, %rdi
	movq	$-1, %rax
	lock
	xaddq	%rax, (%rsi)
	cmpq	$1, %rax
	je	.LBB1_14
.LBB1_15:
	lock
	xaddq	%rdi, (%rsi)
	cmpq	$1, %rdi
	je	.LBB1_16
.LBB1_17:
	movabsq	$PyFloat_FromDouble, %rax
	movaps	%xmm6, %xmm0
	callq	*%rax
.LBB1_2:
	movaps	112(%rsp), %xmm6
	addq	$136, %rsp
	popq	%rdi
	popq	%rsi
	retq
.LBB1_5:
	movabsq	$PyExc_RuntimeError, %rcx
	movabsq	$.const.missing_Environment, %rdx
	movabsq	$PyErr_SetString, %rax
	callq	*%rax
.LBB1_1:
	xorl	%eax, %eax
	jmp	.LBB1_2
.LBB1_14:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%rsi, %rcx
	callq	*%rax
	jmp	.LBB1_15
.LBB1_16:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%rsi, %rcx
	callq	*%rax
	jmp	.LBB1_17
.LBB1_18:
	movabsq	$".const.Fatal_error:_missing__dynfunc.Closure", %rcx
	movabsq	$puts, %rax
	callq	*%rax
	ud2
.Ltmp11:
	.size	"wrapper.<dynamic>.f$6.array(float64,_1d,_C)", .Ltmp11-"wrapper.<dynamic>.f$6.array(float64,_1d,_C)"
	.cfi_endproc

	.type	.const.f,@object
	.section	.rodata,"a",@progbits
.const.f:
	.asciz	"f"
	.size	.const.f, 2

	.type	".const.Fatal_error:_missing__dynfunc.Closure",@object
	.align	16
".const.Fatal_error:_missing__dynfunc.Closure":
	.asciz	"Fatal error: missing _dynfunc.Closure"
	.size	".const.Fatal_error:_missing__dynfunc.Closure", 38

	.type	.const.missing_Environment,@object
	.align	16
.const.missing_Environment:
	.asciz	"missing Environment"
	.size	.const.missing_Environment, 20


	.section	".note.GNU-stack","",@progbits
