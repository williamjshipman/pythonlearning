	.text
	.file	"f"
	.globl	"<dynamic>.f$35.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"<dynamic>.f$35.array(float64,_1d,_C)",@function
"<dynamic>.f$35.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%r15
.Ltmp0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp1:
	.cfi_def_cfa_offset 24
	pushq	%rsi
.Ltmp2:
	.cfi_def_cfa_offset 32
	pushq	%rdi
.Ltmp3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Ltmp4:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
	movaps	%xmm6, 32(%rsp)
.Ltmp5:
	.cfi_def_cfa_offset 96
.Ltmp6:
	.cfi_offset %rbx, -48
.Ltmp7:
	.cfi_offset %rdi, -40
.Ltmp8:
	.cfi_offset %rsi, -32
.Ltmp9:
	.cfi_offset %r14, -24
.Ltmp10:
	.cfi_offset %r15, -16
.Ltmp11:
	.cfi_offset %xmm6, -64
	movq	%r9, %r15
	movq	%rcx, %r14
	movq	136(%rsp), %rbx
	testq	%r15, %r15
	je	.LBB0_2
	lock
	incq	(%r15)
.LBB0_2:
	xorps	%xmm6, %xmm6
	testq	%rbx, %rbx
	jle	.LBB0_5
	movq	152(%rsp), %rsi
	incq	%rbx
	xorps	%xmm6, %xmm6
	movabsq	$numba.npymath.tan, %rdi
	.align	16, 0x90
.LBB0_4:
	movsd	(%rsi), %xmm0
	callq	*%rdi
	addsd	%xmm0, %xmm6
	decq	%rbx
	addq	$8, %rsi
	cmpq	$1, %rbx
	jg	.LBB0_4
.LBB0_5:
	testq	%r15, %r15
	je	.LBB0_8
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r15)
	cmpq	$1, %rax
	je	.LBB0_7
.LBB0_8:
	movsd	%xmm6, (%r14)
	xorl	%eax, %eax
	movaps	32(%rsp), %xmm6
	addq	$48, %rsp
	popq	%rbx
	popq	%rdi
	popq	%rsi
	popq	%r14
	popq	%r15
	retq
.LBB0_7:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r15, %rcx
	callq	*%rax
	jmp	.LBB0_8
.Ltmp12:
	.size	"<dynamic>.f$35.array(float64,_1d,_C)", .Ltmp12-"<dynamic>.f$35.array(float64,_1d,_C)"
	.cfi_endproc

	.globl	"wrapper.<dynamic>.f$35.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"wrapper.<dynamic>.f$35.array(float64,_1d,_C)",@function
"wrapper.<dynamic>.f$35.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%r14
.Ltmp13:
	.cfi_def_cfa_offset 16
	pushq	%rsi
.Ltmp14:
	.cfi_def_cfa_offset 24
	pushq	%rdi
.Ltmp15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Ltmp16:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
	movaps	%xmm6, 112(%rsp)
.Ltmp17:
	.cfi_def_cfa_offset 176
.Ltmp18:
	.cfi_offset %rbx, -40
.Ltmp19:
	.cfi_offset %rdi, -32
.Ltmp20:
	.cfi_offset %rsi, -24
.Ltmp21:
	.cfi_offset %r14, -16
.Ltmp22:
	.cfi_offset %xmm6, -64
	movq	%rcx, %rsi
	leaq	104(%rsp), %rax
	movq	%rax, 32(%rsp)
	movabsq	$.const.f, %rax
	movabsq	$PyArg_UnpackTuple, %rbx
	movl	$1, %r8d
	movl	$1, %r9d
	movq	%rdx, %rcx
	movq	%rax, %rdx
	callq	*%rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	$0, 96(%rsp)
	testl	%eax, %eax
	je	.LBB1_1
	testq	%rsi, %rsi
	je	.LBB1_18
	cmpq	$0, 24(%rsi)
	je	.LBB1_5
	movq	104(%rsp), %rcx
	movabsq	$NRT_adapt_ndarray_from_python, %rax
	leaq	48(%rsp), %rdx
	callq	*%rax
	testl	%eax, %eax
	jne	.LBB1_1
	movq	48(%rsp), %r14
	movq	64(%rsp), %rdi
	movq	80(%rsp), %rbx
	testq	%r14, %r14
	je	.LBB1_9
	lock
	incq	(%r14)
.LBB1_9:
	xorps	%xmm6, %xmm6
	testq	%rdi, %rdi
	jle	.LBB1_12
	incq	%rdi
	xorps	%xmm6, %xmm6
	movabsq	$numba.npymath.tan, %rsi
	.align	16, 0x90
.LBB1_11:
	movsd	(%rbx), %xmm0
	callq	*%rsi
	addsd	%xmm0, %xmm6
	decq	%rdi
	addq	$8, %rbx
	cmpq	$1, %rdi
	jg	.LBB1_11
.LBB1_12:
	testq	%r14, %r14
	je	.LBB1_17
	movq	$-1, %rsi
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r14)
	cmpq	$1, %rax
	je	.LBB1_14
.LBB1_15:
	lock
	xaddq	%rsi, (%r14)
	cmpq	$1, %rsi
	je	.LBB1_16
.LBB1_17:
	movabsq	$PyFloat_FromDouble, %rax
	movaps	%xmm6, %xmm0
	callq	*%rax
.LBB1_2:
	movaps	112(%rsp), %xmm6
	addq	$136, %rsp
	popq	%rbx
	popq	%rdi
	popq	%rsi
	popq	%r14
	retq
.LBB1_5:
	movabsq	$PyExc_RuntimeError, %rcx
	movabsq	$.const.missing_Environment, %rdx
	movabsq	$PyErr_SetString, %rax
	callq	*%rax
.LBB1_1:
	xorl	%eax, %eax
	jmp	.LBB1_2
.LBB1_14:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rcx
	callq	*%rax
	jmp	.LBB1_15
.LBB1_16:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rcx
	callq	*%rax
	jmp	.LBB1_17
.LBB1_18:
	movabsq	$".const.Fatal_error:_missing__dynfunc.Closure", %rcx
	movabsq	$puts, %rax
	callq	*%rax
	ud2
.Ltmp23:
	.size	"wrapper.<dynamic>.f$35.array(float64,_1d,_C)", .Ltmp23-"wrapper.<dynamic>.f$35.array(float64,_1d,_C)"
	.cfi_endproc

	.type	.const.f,@object
	.section	.rodata,"a",@progbits
.const.f:
	.asciz	"f"
	.size	.const.f, 2

	.type	".const.Fatal_error:_missing__dynfunc.Closure",@object
	.align	16
".const.Fatal_error:_missing__dynfunc.Closure":
	.asciz	"Fatal error: missing _dynfunc.Closure"
	.size	".const.Fatal_error:_missing__dynfunc.Closure", 38

	.type	.const.missing_Environment,@object
	.align	16
.const.missing_Environment:
	.asciz	"missing Environment"
	.size	.const.missing_Environment, 20


	.section	".note.GNU-stack","",@progbits
