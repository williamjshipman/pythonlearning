#!/usr/local/bin/python2.7
# encoding: utf-8
'''
numpynumbaspeedtest -- Measure the speed of NumPy and Python math in Numba.

numpynumbaspeedtest is a tool for measuring the speed of different NumPy and
Python maths functions inside and outside of functions compiled using Numba.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
@author:     William John Shipman (williamjshipman)

@copyright:  2016 William John Shipman. All rights reserved.

@license:    GPL v3.0 or later.

@contact:    shipman.william@gmail.com
@deffield    updated: 2016-01-05
'''

from __future__ import print_function, division, unicode_literals
from future.builtins import str, range
from future.utils import iteritems

import seaborn as sns

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import numpy as np
import numba
import math
import pandas as pd
from matplotlib import pyplot as plt

from time import clock

__all__ = []
__version__ = 0.1
__date__ = '2016-01-05'
__updated__ = '2016-01-11'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def map_to_numpy_name(fnname):
    npnames = {'acos':'arccos', 'asin':'arcsin', 'atan':'arctan',
               'acosh':'arccosh', 'asinh':'arcsinh', 'atanh':'arctanh'}
    if fnname in npnames:
        return npnames[fnname]
    else:
        return fnname

def time_individual_function(type_name, function_ptr, num_repeats, num_loops, verbose, test_data):
    durations = np.zeros((num_repeats,))
    if verbose > 0:
        print('\tTiming {:s} function...'.format(type_name))
    for repetition in range(num_repeats):
        tstart = clock()
        for loop in range(num_loops):
            function_ptr(test_data)
        tend = clock()
        durations[repetition] = (tend - tstart) / num_loops
    return durations

def results_to_html(results, fnnames, verbose):
    html_text = '<table style="width:80%;border:none;">\n{rows_text:s}</table>'
    html_row_fmt = '<tr><td>{fnname:}</td><td>{np_python:}</td><td>{math_python}</td><td>{np_numba:}</td><td>{math_numba:}</td><td>{np_arrayop:}</td></tr>\n'
    html_tbl_body = html_row_fmt.format(fnname='Function',
                                        np_python='NumPy (Python)',
                                        math_python='Math (Python)',
                                        np_numba='NumPy (Numba)',
                                        math_numba='Math (Numba)',
                                        np_arrayop='NumPy array operation')
    for fnname in fnnames:
        html_tbl_body += html_row_fmt.format(fnname=fnname,
                                             np_python=results[('python', fnname, 'numpy')].min(),
                                             math_python=results[('python', fnname, 'math')].min(),
                                             np_numba=results[('numba', fnname, 'numpy')].min(),
                                             math_numba=results[('numba', fnname, 'math')].min(),
                                             np_arrayop=results[('array', fnname, 'numpy')].min())
    return html_text.format(rows_text=html_tbl_body)

def plot_results(running_times, function_name_list, verbose):
    speedups = pd.DataFrame(index=function_name_list, columns=['speedup'], dtype=np.float64)
    for fnname in function_name_list:
        speedups['speedup'][fnname] = running_times[('numba', fnname, 'numpy')].min() / running_times[('numba', fnname, 'math')].min()
    ax = speedups.plot(kind='bar', legend=False,
                       title='Speedup achieved using the math package in Numba JIT',
                       fontsize=12)
    ax.title.set_fontsize(16)
    plt.xlabel('Function name', fontsize=14)
    plt.ylabel('Speedup vs. using NumPy', fontsize=14)
    plt.savefig('math_vs_numpy_in_numba_speedup.png', bbox_inches='tight')
    
    for fnname in function_name_list:
        speedups['speedup'][fnname] = running_times[('array', fnname, 'numpy')].min() / running_times[('numba', fnname, 'numpy')].min()
    ax = speedups.plot(kind='bar', legend=False,
                       title='Speedup achieved using NumPy in Numba JIT vs. array operations',
                       fontsize=12)
    ax.title.set_fontsize(16)
    plt.xlabel('Function name', fontsize=14)
    plt.ylabel('Speedup vs NumPy array ops', fontsize=14)
    plt.savefig('numpy_in_numba_vs_numpy_array_ops_speedup.png', bbox_inches='tight')
    
    for fnname in function_name_list:
        speedups['speedup'][fnname] = running_times[('array', fnname, 'numpy')].min() / running_times[('numba', fnname, 'math')].min()
    ax = speedups.plot(kind='bar', legend=False,
                       title='Speedup achieved using math in Numba JIT vs. array operations',
                       fontsize=12)
    ax.title.set_fontsize(16)
    plt.xlabel('Function name', fontsize=14)
    plt.ylabel('Speedup vs. NumPy array ops', fontsize=14)
    plt.savefig('math_in_numba_vs_numpy_array_ops_speedup.png', bbox_inches='tight')
    plt.show()

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split('\n')[1]
    program_license = '''%s

  Created by William John Shipman on %s.
  Copyright 2016 William John Shipman. All rights reserved.

  Licensed under the GNU General Public License v3.0
  http://www.gnu.org/licenses/gpl-3.0.en.html

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='set verbosity level [default: %(default)s]')
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument('-N', '--number-of-elements', dest='num_elements',
                            help='the number of elements to use in the NumPy array. [default: %(default)s]', metavar='num', type=int, default=10**4)
        parser.add_argument('-n', '--number-of-repeats', dest='num_repeats',
                            help='the number of times to repeat the timing loop. [default: %(default)s]', metavar='num', type=int, default=3)
        parser.add_argument('-l', '--number-of-loops', dest='num_loops',
                            help='the number of iterations the timing loop. [default: %(default)s]', metavar='num', type=int, default=10)

        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose
        num_elements = args.num_elements
        num_repeats = args.num_repeats
        num_loops = args.num_loops
        
        function_name_list = ['ceil', 'fabs', 'floor', 'isinf', 'isnan',
                              'trunc', 'exp', 'expm1', 'log', 'log1p',
                              'log10', 'sqrt', 'acos', 'asin', 'atan',
                              'cos', 'sin', 'tan', 'degrees', 'radians',
                              'acosh', 'asinh', 'atanh', 'cosh', 'sinh',
                              'tanh']
#         function_name_list = ['exp', 'log', 'ceil']

        if verbose > 0:
            print('Verbose mode on')
            print('Python version: {:s}'.format(sys.version))
            print('NumPy version:  {:s}'.format(np.version.full_version))
            print('Numba version:  {:s}'.format(numba.__version__))
            print('The following functions will be tested: ', end='')
            for fnname in function_name_list[:-1]:
                print(fnname, end=', ')
            print(function_name_list[-1] + '.')
            print('Configuration:')
            print('\tSize of NumPy array: {:} elements.'.format(num_elements))
            print('\tNumber of repeats:   {:}.'.format(num_repeats))
            print('\tTimes averaged over: {:} loops.'.format(num_loops))
        
        running_times = {}
        for fnname in function_name_list:
            if fnname in ['acos', 'asin']:
                test_data = np.random.uniform(low=-1.0, high=+1.0, size=num_elements)
            elif fnname == 'acosh':
                test_data = np.random.uniform(low=1.0, high=1.0e3, size=num_elements)
            elif fnname == 'atanh':
                test_data = np.random.uniform(low=-0.9, high=+0.9, size=num_elements)
            elif fnname in ['log', 'lgamma', 'log10']:
                test_data = np.random.uniform(low=np.finfo(np.float64).eps, high=1.0e3, size=num_elements)
            elif fnname == 'log1p':
                test_data = np.random.uniform(low=-1.0 + np.finfo(np.float64).eps, high=1.0e3, size=num_elements)
            elif fnname == 'sqrt':
                test_data = np.random.uniform(low=0.0, high=1.0e3, size=num_elements)
            elif fnname in ['exp', 'expm1']:
                test_data = np.random.uniform(low=-1.0e3, high=math.pow(10, 2.85), size=num_elements)
            elif fnname in ['cosh', 'sinh']:
                test_data = np.random.uniform(low=-math.pow(10, 2.85), high=math.pow(10, 2.85), size=num_elements)
            else:
                test_data = np.random.uniform(low=-1.0e3, high=1.0e3, size=num_elements)
            code = '''
import {module:s}
from numba import jit
def f(x):
    sum = 0.0
    for idx in range(x.size):
        sum += {module:s}.{function:s}(x[idx])
    return sum
jit_f = jit(f)
array_fn = {module:s}.{function:s}
'''
            for module in ['numpy', 'math']:
                if module == 'numpy':
                    used_fnname = map_to_numpy_name(fnname)
                else:
                    used_fnname = fnname
                if verbose > 0:
                    print('{module:s}.{function:s}:'.format(module=module, function=used_fnname))
                    print('\tCompiling code...')
                compiled_code = compile(code.format(module=module, function=used_fnname), '<string>', 'exec')
                namespace = {}
                if verbose > 0:
                    print('\tExecuting...')
                exec(compiled_code, namespace)

                running_times[('python', fnname, module)] = time_individual_function('Python', namespace['f'], num_repeats, num_loops, verbose, test_data)
                running_times[('numba', fnname, module)] = time_individual_function('Numba JITted', namespace['jit_f'], num_repeats, num_loops, verbose, test_data)
                if module == 'numpy':
                    running_times[('array', fnname, module)] = time_individual_function('array operation version of', namespace['array_fn'], num_repeats, num_loops, verbose, test_data)
                else:
                    running_times[('array', fnname, module)] = np.ones((num_repeats,)) * np.NaN
                
                print(' Saving Numba JIT ASM code...', end='')
                for signature, asm_code in iteritems(namespace['jit_f'].inspect_asm()):
                    with open('{module:}.{function:}-{signature:}.asm'.format(module=module, function=used_fnname, signature=signature), 'w') as asm_file:
                        asm_file.write(asm_code)
                print('Done.')
                
                print('{module:s}.{function:s}: {pytime:} seconds (Python), {numbatime:} seconds (Numba), {arrayoptime:} seconds (array)'.format(module=module, function=used_fnname, pytime=running_times[('python', fnname, module)].min(), numbatime=running_times[('numba', fnname, module)].min(), arrayoptime=running_times[('array', fnname, module)].min()))

        print('\nHTML formatted results:\n')
        html_results = results_to_html(running_times, function_name_list, verbose)
        print(html_results)
        with open('running-time-results.html', 'w') as html_file:
            html_file.write(html_results)
        
        plot_results(running_times, function_name_list, verbose)
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        if DEBUG or TESTRUN:
            raise
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == '__main__':
    if DEBUG:
        sys.argv.append('-v')
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'numpynumbaspeedtest_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open('profile_stats.txt', 'wb')
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())