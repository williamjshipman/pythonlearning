	.text
	.file	"f"
	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI0_0:
	.quad	4633260481411531256
	.text
	.globl	"<dynamic>.f$38.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"<dynamic>.f$38.array(float64,_1d,_C)",@function
"<dynamic>.f$38.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%rbx
.Ltmp0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Ltmp1:
	.cfi_def_cfa_offset 32
.Ltmp2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	je	.LBB0_2
	lock
	incq	(%rcx)
.LBB0_2:
	xorps	%xmm2, %xmm2
	testq	%r9, %r9
	jle	.LBB0_5
	movq	40(%rsp), %rax
	incq	%r9
	xorps	%xmm2, %xmm2
	movabsq	$.LCPI0_0, %rdx
	movsd	(%rdx), %xmm0
	.align	16, 0x90
.LBB0_4:
	movsd	(%rax), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm2
	decq	%r9
	addq	$8, %rax
	cmpq	$1, %r9
	jg	.LBB0_4
.LBB0_5:
	testq	%rcx, %rcx
	je	.LBB0_8
	movq	$-1, %rax
	lock
	xaddq	%rax, (%rcx)
	cmpq	$1, %rax
	je	.LBB0_7
.LBB0_8:
	movsd	%xmm2, (%rbx)
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB0_7:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%rcx, %rdi
	movsd	%xmm2, 8(%rsp)
	callq	*%rax
	movsd	8(%rsp), %xmm2
	jmp	.LBB0_8
.Ltmp3:
	.size	"<dynamic>.f$38.array(float64,_1d,_C)", .Ltmp3-"<dynamic>.f$38.array(float64,_1d,_C)"
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI1_0:
	.quad	4633260481411531256
	.text
	.globl	"wrapper.<dynamic>.f$38.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"wrapper.<dynamic>.f$38.array(float64,_1d,_C)",@function
"wrapper.<dynamic>.f$38.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%r14
.Ltmp4:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Ltmp5:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Ltmp6:
	.cfi_def_cfa_offset 112
.Ltmp7:
	.cfi_offset %rbx, -24
.Ltmp8:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movabsq	$.const.f, %r9
	movabsq	$PyArg_UnpackTuple, %r10
	leaq	80(%rsp), %r8
	movl	$1, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	movq	%r9, %rsi
	callq	*%r10
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	$0, 64(%rsp)
	testl	%eax, %eax
	je	.LBB1_1
	testq	%rbx, %rbx
	je	.LBB1_18
	cmpq	$0, 24(%rbx)
	je	.LBB1_5
	movq	80(%rsp), %rdi
	movabsq	$NRT_adapt_ndarray_from_python, %rax
	leaq	16(%rsp), %rsi
	callq	*%rax
	testl	%eax, %eax
	jne	.LBB1_1
	movq	16(%rsp), %r14
	movq	32(%rsp), %rax
	movq	48(%rsp), %rcx
	testq	%r14, %r14
	je	.LBB1_9
	lock
	incq	(%r14)
.LBB1_9:
	xorps	%xmm0, %xmm0
	testq	%rax, %rax
	jle	.LBB1_12
	incq	%rax
	xorps	%xmm0, %xmm0
	movabsq	$.LCPI1_0, %rdx
	movsd	(%rdx), %xmm2
	.align	16, 0x90
.LBB1_11:
	movsd	(%rcx), %xmm1
	mulsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	decq	%rax
	addq	$8, %rcx
	cmpq	$1, %rax
	jg	.LBB1_11
.LBB1_12:
	testq	%r14, %r14
	je	.LBB1_17
	movq	$-1, %rbx
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r14)
	cmpq	$1, %rax
	je	.LBB1_14
.LBB1_15:
	lock
	xaddq	%rbx, (%r14)
	cmpq	$1, %rbx
	je	.LBB1_16
.LBB1_17:
	movabsq	$PyFloat_FromDouble, %rax
	callq	*%rax
.LBB1_2:
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_5:
	movabsq	$PyExc_RuntimeError, %rdi
	movabsq	$.const.missing_Environment, %rsi
	movabsq	$PyErr_SetString, %rax
	callq	*%rax
.LBB1_1:
	xorl	%eax, %eax
	jmp	.LBB1_2
.LBB1_14:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)
	callq	*%rax
	movsd	8(%rsp), %xmm0
	jmp	.LBB1_15
.LBB1_16:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)
	callq	*%rax
	movsd	8(%rsp), %xmm0
	jmp	.LBB1_17
.LBB1_18:
	movabsq	$".const.Fatal_error:_missing__dynfunc.Closure", %rdi
	movabsq	$puts, %rax
	callq	*%rax
.Ltmp9:
	.size	"wrapper.<dynamic>.f$38.array(float64,_1d,_C)", .Ltmp9-"wrapper.<dynamic>.f$38.array(float64,_1d,_C)"
	.cfi_endproc

	.type	.const.f,@object
	.section	.rodata,"a",@progbits
.const.f:
	.asciz	"f"
	.size	.const.f, 2

	.type	".const.Fatal_error:_missing__dynfunc.Closure",@object
	.align	16
".const.Fatal_error:_missing__dynfunc.Closure":
	.asciz	"Fatal error: missing _dynfunc.Closure"
	.size	".const.Fatal_error:_missing__dynfunc.Closure", 38

	.type	.const.missing_Environment,@object
	.align	16
.const.missing_Environment:
	.asciz	"missing Environment"
	.size	.const.missing_Environment, 20


	.section	".note.GNU-stack","",@progbits
