	.text
	.file	"f"
	.globl	"<dynamic>.f$51.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"<dynamic>.f$51.array(float64,_1d,_C)",@function
"<dynamic>.f$51.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%r15
.Ltmp0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Ltmp2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Ltmp3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Ltmp4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Ltmp5:
	.cfi_def_cfa_offset 64
.Ltmp6:
	.cfi_offset %rbx, -48
.Ltmp7:
	.cfi_offset %r12, -40
.Ltmp8:
	.cfi_offset %r13, -32
.Ltmp9:
	.cfi_offset %r14, -24
.Ltmp10:
	.cfi_offset %r15, -16
	movq	%r9, %r12
	movq	%rcx, %r15
	movq	%rdi, %r14
	testq	%r15, %r15
	je	.LBB0_2
	lock
	incq	(%r15)
.LBB0_2:
	xorps	%xmm0, %xmm0
	testq	%r12, %r12
	jle	.LBB0_5
	movq	72(%rsp), %rbx
	incq	%r12
	xorps	%xmm0, %xmm0
	movabsq	$numba.npymath.tanh, %r13
	.align	16, 0x90
.LBB0_4:
	movsd	%xmm0, 8(%rsp)
	movsd	(%rbx), %xmm0
	callq	*%r13
	movsd	8(%rsp), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)
	movsd	8(%rsp), %xmm0
	decq	%r12
	addq	$8, %rbx
	cmpq	$1, %r12
	jg	.LBB0_4
.LBB0_5:
	testq	%r15, %r15
	je	.LBB0_8
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r15)
	cmpq	$1, %rax
	je	.LBB0_7
.LBB0_8:
	movsd	%xmm0, (%r14)
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_7:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r15, %rdi
	movsd	%xmm0, 8(%rsp)
	callq	*%rax
	movsd	8(%rsp), %xmm0
	jmp	.LBB0_8
.Ltmp11:
	.size	"<dynamic>.f$51.array(float64,_1d,_C)", .Ltmp11-"<dynamic>.f$51.array(float64,_1d,_C)"
	.cfi_endproc

	.globl	"wrapper.<dynamic>.f$51.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"wrapper.<dynamic>.f$51.array(float64,_1d,_C)",@function
"wrapper.<dynamic>.f$51.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%r15
.Ltmp12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp13:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Ltmp14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Ltmp15:
	.cfi_def_cfa_offset 40
	subq	$88, %rsp
.Ltmp16:
	.cfi_def_cfa_offset 128
.Ltmp17:
	.cfi_offset %rbx, -40
.Ltmp18:
	.cfi_offset %r12, -32
.Ltmp19:
	.cfi_offset %r14, -24
.Ltmp20:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movabsq	$.const.f, %r9
	movabsq	$PyArg_UnpackTuple, %r10
	leaq	80(%rsp), %r8
	movl	$1, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	movq	%r9, %rsi
	callq	*%r10
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	$0, 64(%rsp)
	testl	%eax, %eax
	je	.LBB1_1
	testq	%rbx, %rbx
	je	.LBB1_18
	cmpq	$0, 24(%rbx)
	je	.LBB1_5
	movq	80(%rsp), %rdi
	movabsq	$NRT_adapt_ndarray_from_python, %rax
	leaq	16(%rsp), %rsi
	callq	*%rax
	testl	%eax, %eax
	jne	.LBB1_1
	movq	16(%rsp), %r14
	movq	32(%rsp), %rbx
	movq	48(%rsp), %r15
	testq	%r14, %r14
	je	.LBB1_9
	lock
	incq	(%r14)
.LBB1_9:
	xorps	%xmm0, %xmm0
	testq	%rbx, %rbx
	jle	.LBB1_12
	incq	%rbx
	xorps	%xmm0, %xmm0
	movabsq	$numba.npymath.tanh, %r12
	.align	16, 0x90
.LBB1_11:
	movsd	%xmm0, 8(%rsp)
	movsd	(%r15), %xmm0
	callq	*%r12
	movsd	8(%rsp), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)
	movsd	8(%rsp), %xmm0
	decq	%rbx
	addq	$8, %r15
	cmpq	$1, %rbx
	jg	.LBB1_11
.LBB1_12:
	testq	%r14, %r14
	je	.LBB1_17
	movq	$-1, %rbx
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r14)
	cmpq	$1, %rax
	je	.LBB1_14
.LBB1_15:
	lock
	xaddq	%rbx, (%r14)
	cmpq	$1, %rbx
	je	.LBB1_16
.LBB1_17:
	movabsq	$PyFloat_FromDouble, %rax
	callq	*%rax
.LBB1_2:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_5:
	movabsq	$PyExc_RuntimeError, %rdi
	movabsq	$.const.missing_Environment, %rsi
	movabsq	$PyErr_SetString, %rax
	callq	*%rax
.LBB1_1:
	xorl	%eax, %eax
	jmp	.LBB1_2
.LBB1_14:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)
	callq	*%rax
	movsd	8(%rsp), %xmm0
	jmp	.LBB1_15
.LBB1_16:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)
	callq	*%rax
	movsd	8(%rsp), %xmm0
	jmp	.LBB1_17
.LBB1_18:
	movabsq	$".const.Fatal_error:_missing__dynfunc.Closure", %rdi
	movabsq	$puts, %rax
	callq	*%rax
.Ltmp21:
	.size	"wrapper.<dynamic>.f$51.array(float64,_1d,_C)", .Ltmp21-"wrapper.<dynamic>.f$51.array(float64,_1d,_C)"
	.cfi_endproc

	.type	.const.f,@object
	.section	.rodata,"a",@progbits
.const.f:
	.asciz	"f"
	.size	.const.f, 2

	.type	".const.Fatal_error:_missing__dynfunc.Closure",@object
	.align	16
".const.Fatal_error:_missing__dynfunc.Closure":
	.asciz	"Fatal error: missing _dynfunc.Closure"
	.size	".const.Fatal_error:_missing__dynfunc.Closure", 38

	.type	.const.missing_Environment,@object
	.align	16
.const.missing_Environment:
	.asciz	"missing Environment"
	.size	.const.missing_Environment, 20


	.section	".note.GNU-stack","",@progbits
