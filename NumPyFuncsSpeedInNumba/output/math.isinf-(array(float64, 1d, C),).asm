	.text
	.file	"f"
	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI0_0:
	.quad	9218868437227405312
.LCPI0_1:
	.quad	-4503599627370496
	.text
	.globl	"<dynamic>.f$8.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"<dynamic>.f$8.array(float64,_1d,_C)",@function
"<dynamic>.f$8.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%r14
.Ltmp0:
	.cfi_def_cfa_offset 16
	pushq	%rsi
.Ltmp1:
	.cfi_def_cfa_offset 24
	pushq	%rdi
.Ltmp2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Ltmp3:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
	movaps	%xmm6, 32(%rsp)
.Ltmp4:
	.cfi_def_cfa_offset 96
.Ltmp5:
	.cfi_offset %rbx, -40
.Ltmp6:
	.cfi_offset %rdi, -32
.Ltmp7:
	.cfi_offset %rsi, -24
.Ltmp8:
	.cfi_offset %r14, -16
.Ltmp9:
	.cfi_offset %xmm6, -64
	movq	%rcx, %r14
	movq	136(%rsp), %rax
	testq	%r9, %r9
	je	.LBB0_2
	lock
	incq	(%r9)
.LBB0_2:
	xorps	%xmm6, %xmm6
	testq	%rax, %rax
	jle	.LBB0_5
	movq	152(%rsp), %rcx
	incq	%rax
	xorps	%xmm6, %xmm6
	movabsq	$.LCPI0_0, %rdi
	movabsq	$.LCPI0_1, %rsi
	movsd	(%rsi), %xmm0
	.align	16, 0x90
.LBB0_4:
	movsd	(%rcx), %xmm1
	ucomisd	(%rdi), %xmm1
	setae	%dl
	ucomisd	%xmm1, %xmm0
	setae	%bl
	orb	%dl, %bl
	movzbl	%bl, %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	addsd	%xmm1, %xmm6
	decq	%rax
	addq	$8, %rcx
	cmpq	$1, %rax
	jg	.LBB0_4
.LBB0_5:
	testq	%r9, %r9
	je	.LBB0_8
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r9)
	cmpq	$1, %rax
	je	.LBB0_7
.LBB0_8:
	movsd	%xmm6, (%r14)
	xorl	%eax, %eax
	movaps	32(%rsp), %xmm6
	addq	$56, %rsp
	popq	%rbx
	popq	%rdi
	popq	%rsi
	popq	%r14
	retq
.LBB0_7:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r9, %rcx
	callq	*%rax
	jmp	.LBB0_8
.Ltmp10:
	.size	"<dynamic>.f$8.array(float64,_1d,_C)", .Ltmp10-"<dynamic>.f$8.array(float64,_1d,_C)"
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI1_0:
	.quad	9218868437227405312
.LCPI1_1:
	.quad	-4503599627370496
	.text
	.globl	"wrapper.<dynamic>.f$8.array(float64,_1d,_C)"
	.align	16, 0x90
	.type	"wrapper.<dynamic>.f$8.array(float64,_1d,_C)",@function
"wrapper.<dynamic>.f$8.array(float64,_1d,_C)":
	.cfi_startproc
	pushq	%r14
.Ltmp11:
	.cfi_def_cfa_offset 16
	pushq	%rsi
.Ltmp12:
	.cfi_def_cfa_offset 24
	pushq	%rdi
.Ltmp13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Ltmp14:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
	movaps	%xmm6, 112(%rsp)
.Ltmp15:
	.cfi_def_cfa_offset 176
.Ltmp16:
	.cfi_offset %rbx, -40
.Ltmp17:
	.cfi_offset %rdi, -32
.Ltmp18:
	.cfi_offset %rsi, -24
.Ltmp19:
	.cfi_offset %r14, -16
.Ltmp20:
	.cfi_offset %xmm6, -64
	movq	%rcx, %rsi
	leaq	104(%rsp), %rax
	movq	%rax, 32(%rsp)
	movabsq	$.const.f, %rax
	movabsq	$PyArg_UnpackTuple, %rdi
	movl	$1, %r8d
	movl	$1, %r9d
	movq	%rdx, %rcx
	movq	%rax, %rdx
	callq	*%rdi
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	$0, 96(%rsp)
	testl	%eax, %eax
	je	.LBB1_1
	testq	%rsi, %rsi
	je	.LBB1_18
	cmpq	$0, 24(%rsi)
	je	.LBB1_5
	movq	104(%rsp), %rcx
	movabsq	$NRT_adapt_ndarray_from_python, %rax
	leaq	48(%rsp), %rdx
	callq	*%rax
	testl	%eax, %eax
	jne	.LBB1_1
	movq	48(%rsp), %r14
	movq	64(%rsp), %rax
	movq	80(%rsp), %rcx
	testq	%r14, %r14
	je	.LBB1_9
	lock
	incq	(%r14)
.LBB1_9:
	xorps	%xmm6, %xmm6
	testq	%rax, %rax
	jle	.LBB1_12
	incq	%rax
	xorps	%xmm6, %xmm6
	movabsq	$.LCPI1_0, %rsi
	movabsq	$.LCPI1_1, %rdi
	movsd	(%rdi), %xmm0
	.align	16, 0x90
.LBB1_11:
	movsd	(%rcx), %xmm1
	ucomisd	(%rsi), %xmm1
	setae	%dl
	ucomisd	%xmm1, %xmm0
	setae	%bl
	orb	%dl, %bl
	movzbl	%bl, %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	addsd	%xmm1, %xmm6
	decq	%rax
	addq	$8, %rcx
	cmpq	$1, %rax
	jg	.LBB1_11
.LBB1_12:
	testq	%r14, %r14
	je	.LBB1_17
	movq	$-1, %rdi
	movq	$-1, %rax
	lock
	xaddq	%rax, (%r14)
	cmpq	$1, %rax
	je	.LBB1_14
.LBB1_15:
	lock
	xaddq	%rdi, (%r14)
	cmpq	$1, %rdi
	je	.LBB1_16
.LBB1_17:
	movabsq	$PyFloat_FromDouble, %rax
	movaps	%xmm6, %xmm0
	callq	*%rax
.LBB1_2:
	movaps	112(%rsp), %xmm6
	addq	$136, %rsp
	popq	%rbx
	popq	%rdi
	popq	%rsi
	popq	%r14
	retq
.LBB1_5:
	movabsq	$PyExc_RuntimeError, %rcx
	movabsq	$.const.missing_Environment, %rdx
	movabsq	$PyErr_SetString, %rax
	callq	*%rax
.LBB1_1:
	xorl	%eax, %eax
	jmp	.LBB1_2
.LBB1_14:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rcx
	callq	*%rax
	jmp	.LBB1_15
.LBB1_16:
	movabsq	$NRT_MemInfo_call_dtor, %rax
	movq	%r14, %rcx
	callq	*%rax
	jmp	.LBB1_17
.LBB1_18:
	movabsq	$".const.Fatal_error:_missing__dynfunc.Closure", %rcx
	movabsq	$puts, %rax
	callq	*%rax
	ud2
.Ltmp21:
	.size	"wrapper.<dynamic>.f$8.array(float64,_1d,_C)", .Ltmp21-"wrapper.<dynamic>.f$8.array(float64,_1d,_C)"
	.cfi_endproc

	.type	.const.f,@object
	.section	.rodata,"a",@progbits
.const.f:
	.asciz	"f"
	.size	.const.f, 2

	.type	".const.Fatal_error:_missing__dynfunc.Closure",@object
	.align	16
".const.Fatal_error:_missing__dynfunc.Closure":
	.asciz	"Fatal error: missing _dynfunc.Closure"
	.size	".const.Fatal_error:_missing__dynfunc.Closure", 38

	.type	.const.missing_Environment,@object
	.align	16
.const.missing_Environment:
	.asciz	"missing Environment"
	.size	.const.missing_Environment, 20


	.section	".note.GNU-stack","",@progbits
