#!/usr/local/bin/python2.7
# encoding: utf-8
'''
filter2dimage -- Demos six ways to filter a 2D image.

filter2dimage is a demo of eight ways to filter a 2D image.  Comparison is
based on the running time of the different methods.

@author:     William John Shipman
        
@copyright:  2013 William John Shipman. All rights reserved.
        
@license:    GNU General Public License version 3 or later.

@deffield    updated: 2013-12-25
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import numpy as np

import scipy.ndimage as ndimage
import scipy.ndimage.filters as ndfilt

from skimage.io import imsave as imsave
from skimage.filter import denoise_bilateral as skimg_bilateral

from sklearn.feature_extraction import image as sklrnimg

import matplotlib.pyplot as plt

import numba

from datetime import datetime

import cv2

__all__ = []
__version__ = 0.1
__date__ = '2013-12-21'
__updated__ = '2013-12-25'

DEBUG = 0
TESTRUN = 0
PROFILE = 0

range_sigma = 20.0
range_coefficient = 1.0 / (np.sqrt(2.0 * np.pi) * range_sigma)
range_divisor = 2.0 * range_sigma * range_sigma

spatial_X = np.array(
    [[-2., -1., 0., 1., 2.],
     [-2., -1., 0., 1., 2.],
     [-2., -1., 0., 1., 2.],
     [-2., -1., 0., 1., 2.],
     [-2., -1., 0., 1., 2.]])
spatial_Y = np.array(
    [[-2., -2., -2., -2., -2.],
     [-1., -1., -1., -1., -1.],
     [0., 0., 0., 0., 0.],
     [1., 1., 1., 1., 1.],
     [2., 2., 2., 2., 2.]])
spatial_sigma = 20.0
spatial_coefficient = 1.0 / (np.sqrt(2.0 * np.pi) * spatial_sigma)
spatial_gaussian_weights = (spatial_coefficient * np.exp(-(spatial_X ** 2.0 + spatial_Y ** 2.0) / (2.0 * spatial_sigma * spatial_sigma))).flatten()

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

class BilateralFiltersBase(object):
    '''Base class that collects the spatial weights for the bilateral filter.'''
    def __init__(self):
        '''Pre-calculate the filter weights for the spatial part of the blur.'''
        spatial_X = np.array(
            [[-2., -1., 0., 1., 2.],
             [-2., -1., 0., 1., 2.],
             [-2., -1., 0., 1., 2.],
             [-2., -1., 0., 1., 2.],
             [-2., -1., 0., 1., 2.]])
        spatial_Y = np.array(
            [[-2., -2., -2., -2., -2.],
             [-1., -1., -1., -1., -1.],
             [0., 0., 0., 0., 0.],
             [1., 1., 1., 1., 1.],
             [2., 2., 2., 2., 2.]])
        spatial_sigma = 20.0
        self.spatial_gaussian_weights = ((1.0 / (np.sqrt(2.0 * np.pi) * spatial_sigma)) * np.exp(-(spatial_X ** 2.0 + spatial_Y ** 2.0) / (2.0 * spatial_sigma * spatial_sigma))).flatten()

class BilateralFilters(BilateralFiltersBase):
    '''Multiple implementations of the bilateral filter for benchmarking.
    
    Numba IS NOT used in this class.
    
    '''

    def naive_2D_filter_v1(self, img):
        '''Basic quadruple for loop bilateral filtering implementation.
        
        Arguments:
        img -- NumPy 2D array to filter.
        
        Returns: 2D NumPy array of the same type as img.
        
        '''
        output = np.zeros(img.shape, dtype=img.dtype)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                centre = img[row, col]
                idx = 0
                total_weight = 0.0
                sum_neighbours = 0.0
                for r in xrange(row-2, row+3):
                    for c in xrange(col-2, col+3):
                        if ((r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1])):
                            pixel = img[r,c]
                        else:
                            pixel = 0.0
                        dr = centre - pixel
                        filter_weight = range_coefficient * np.exp(-1.0 * dr * dr / range_divisor) * self.spatial_gaussian_weights[idx]
                        sum_neighbours += (filter_weight * pixel)
                        total_weight += filter_weight
                        idx += 1
                output[row, col] = sum_neighbours / total_weight
        return output
    
    def bilateral_kernel(self, input):
        '''The core/kernel of the bilateral filter.
        
        Arguments:
        input -- 1D NumPy array representing a 5x5 window around the current pixel.
        
        Returns: The weighted pixel intensity.
        
        '''
        centre = input[12]
        range = input - centre
        range_contrib = range_coefficient * np.exp(-1.0 * range * range / range_divisor)
        total_weights = range_contrib * spatial_gaussian_weights
        # Normalise the blur coefficients so that the image's brightness is not
        # increased or decreased.
        total_weights /= total_weights.sum()
        return np.sum(input * total_weights)
    
    def naive_2D_filter_v3(self, img):
        output = np.zeros(img.shape, dtype=img.dtype)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                # Using an array of indices requires that the first row of
                # indices contains the row number, while the second row of
                # indices contains the column number.
                idx = np.array([[row+r for r in xrange(-2,3) for t in xrange(-2,3)], [col+c for t in xrange(-2,3) for c in xrange(-2,3)]])
                idx_valid = (idx[0,:] >= 0) & (idx[0,:] < img.shape[0]) & (idx[1,:] >= 0) & (idx[1,:] < img.shape[1])
                idx = idx[:,idx_valid]
                window = np.zeros((25,))
                window[idx_valid] = img[idx[0,:], idx[1,:]]
                output[row, col] = self.bilateral_kernel(window)
        return output
    
    def naive_2D_filter_v4(self, img):
        output = np.zeros(img.shape, dtype=img.dtype)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                idx = 0
                window = np.zeros((25,))
                for r in xrange(row-2, row+3):
                    for c in xrange(col-2, col+3):
                        if (r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1]):
                            window[idx] = img[r,c]
                        else:
                            window[idx] = 0.0
                        idx += 1
                output[row, col] = self.bilateral_kernel(window)
        return output
    
    def scipy_gen_filter(self, img):
        output = np.zeros(img.shape, dtype=img.dtype)
        ndfilt.generic_filter(img, self.bilateral_kernel, [5, 5], output=output, mode='constant', cval=0.0)
        return output
    
    def sklearn_2d_filter(self, img):
        '''Filter image using scikit-learn extract_patches_2d.
        
        Arguments:
        img -- 2D NumPy array representing the input image (gray scale).
        
        Returns: The filtered image as a NumPy 2D array.  The array contains
        4 fewer rows and columns than img as it looses 2 layers of pixels on
        the edges of the image.  This is because extract_patches_2d does not
        return patches for border pixels where some pixels in the window lie
        outside of the image.
        
        '''
        all_windows = sklrnimg.extract_patches_2d(img, (5, 5))
        output_patches = np.zeros((all_windows.shape[0], 1, 1))
        sum_of_weights = np.zeros((all_windows.shape[0], 1, 1))
        
        idx = 0
        for r in xrange(5):
            for c in xrange(5):
                dr = all_windows[:,3,3] - all_windows[:,r,c]
                weight = (range_coefficient * np.exp(-1.0 * dr * dr / range_divisor) * self.spatial_gaussian_weights[idx]).reshape(output_patches.shape)
                idx+=1
                sum_of_weights += weight
                output_patches += all_windows[:,r,c].reshape(weight.shape) * weight
        
        output_patches /= sum_of_weights
        output = sklrnimg.reconstruct_from_patches_2d(output_patches, (img.shape[0] - 4, img.shape[1] - 4))
        return output

@numba.jit
class BilateralFiltersNumba(BilateralFiltersBase):
    '''Multiple implementations of the bilateral filter for benchmarking.
    
    Numba IS used in this class.
    
    '''

    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def naive_2D_filter_v1(self, img):
        '''Numba equivalent to BilateralFilters.naive_2D_filter_v1.
        
        The key difference is that Numba would only work correctly when the
        variable scalar_zero was introduced to initialise variables with
        np.float64(0.0) instead of normal 0.0.
        
        '''
        output = np.zeros(img.shape, dtype=img.dtype)
        scalar_zero = np.float64(0.0)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                centre = img[row, col]
                idx = 0
                total_weight = scalar_zero
                sum_neighbours = scalar_zero
                for r in xrange(row-2, row+3):
                    for c in xrange(col-2, col+3):
                        if ((r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1])):
                            pixel = img[r,c]
                        else:
                            pixel = 0.0
                        dr = centre - pixel
                        filter_weight = range_coefficient * np.exp(-1.0 * dr * dr / range_divisor) * self.spatial_gaussian_weights[idx]
                        sum_neighbours += (filter_weight * pixel)
                        total_weight += filter_weight
                        idx += 1
                output[row, col] = sum_neighbours / total_weight
        return output

    @numba.jit(numba.float64(numba.object_, numba.float64[:,:], numba.int_, numba.int_))
    def naive_2D_filter_v2_kernel(self, img, row, col):
        centre = img[row, col]
        idx = 0
        total_weight = 0.0
        sum_neighbours = 0.0 
        for r in xrange(row-2, row+3):
            for c in xrange(col-2, col+3):
                if ((r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1])):
                    pixel = img[r,c]
                else:
                    pixel = 0.0
                dr = centre - pixel
                filter_weight = range_coefficient * np.exp((-1.0 * dr * dr / range_divisor)) * self.spatial_gaussian_weights[idx]
                sum_neighbours += (filter_weight * pixel)
                total_weight += filter_weight
                idx += 1
        return sum_neighbours / total_weight

    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def naive_2D_filter_v2(self, img):
        output = np.zeros(img.shape, dtype=np.float64)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                output[row, col] = self.naive_2D_filter_v2_kernel(img, row, col)
        return output

    @numba.jit(numba.float64(numba.object_, numba.float64[:]))
    def bilateral_kernel(self, input):
        centre = input[12]
        range = input - centre
        range_contrib = range_coefficient * np.exp(-1.0 * range * range / range_divisor)
        total_weights = range_contrib * self.spatial_gaussian_weights
        total_weights = total_weights / total_weights.sum()
        return np.sum(input * total_weights)

    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def naive_2D_filter_v4(self, img):
        output = np.zeros(img.shape, dtype=np.float64)
        window = np.zeros((25,), dtype=np.float64)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                idx = 0
                for r in xrange(row-2, row+3):
                    for c in xrange(col-2, col+3):
                        if (r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1]):
                            window[idx] = img[r,c]
                        else:
                            window[idx] = 0.0
                        idx += 1
                output[row, col] = self.bilateral_kernel(window)
        return output
    
    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def scipy_gen_filter(self, img):
        output = np.zeros(img.shape, dtype=img.dtype)
        ndfilt.generic_filter(img, self.bilateral_kernel, [5, 5], output=output, mode='constant', cval=0.0)
        return output
    
    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def sklearn_2d_filter(self, img):
        all_windows = sklrnimg.extract_patches_2d(img, (5, 5))
        output_patches = np.zeros((all_windows.shape[0], 1, 1))
        sum_of_weights = np.zeros((all_windows.shape[0], 1, 1))
        
        idx = 0
        for r in xrange(5):
            for c in xrange(5):
                dr = all_windows[:,3,3] - all_windows[:,r,c]
                weight = (range_coefficient * np.exp(-1.0 * dr * dr / range_divisor) * self.spatial_gaussian_weights[idx]).reshape(output_patches.shape)
                idx+=1
                sum_of_weights += weight
                output_patches += all_windows[:,r,c].reshape(weight.shape) * weight
        
        output_patches /= sum_of_weights
        output = sklrnimg.reconstruct_from_patches_2d(output_patches, (img.shape[0] - 4, img.shape[1] - 4))
        return output

@numba.jit
class BilateralFiltersNumba_v2(object):
    '''Multiple implementations of the bilateral filter for benchmarking.
    
    Numba IS used in this class.  Version 2 builds on BilateralFiltersNumba,
    fixing some speed issues.
    
    '''

    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def naive_2D_filter_v1(self, img):
        '''Numba equivalent to BilateralFilters.naive_2D_filter_v1.
        
        The key difference is that Numba would only work correctly when the
        variable scalar_zero was introduced to initialise variables with
        np.float64(0.0) instead of normal 0.0.
        
        '''
        output = np.zeros(img.shape, dtype=img.dtype)
        scalar_zero = np.float64(0.0)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                centre = img[row, col]
                idx = 0
                total_weight = 0.0
                sum_neighbours = 0.0
                for r in xrange(row-2, row+3):
                    for c in xrange(col-2, col+3):
                        if ((r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1])):
                            pixel = img[r,c]
                        else:
                            pixel = 0.0
                        dr = centre - pixel
                        filter_weight = range_coefficient * np.exp(-1.0 * dr * dr / range_divisor) * spatial_gaussian_weights[idx]
                        sum_neighbours += (filter_weight * pixel)
                        total_weight += filter_weight
                        idx += 1
                output[row, col] = sum_neighbours / total_weight
        return output

    @numba.jit(numba.float64(numba.object_, numba.float64[:,:], numba.int_, numba.int_))
    def naive_2D_filter_v2_kernel(self, img, row, col):
        centre = img[row, col]
        idx = 0
        total_weight = 0.0
        sum_neighbours = 0.0 
        for r in xrange(row-2, row+3):
            for c in xrange(col-2, col+3):
                if ((r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1])):
                    pixel = img[r,c]
                else:
                    pixel = 0.0
                dr = centre - pixel
                filter_weight = range_coefficient * np.exp((-1.0 * dr * dr / range_divisor)) * spatial_gaussian_weights[idx]
                sum_neighbours += (filter_weight * pixel)
                total_weight += filter_weight
                idx += 1
        return sum_neighbours / total_weight

    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def naive_2D_filter_v2(self, img):
        output = np.zeros(img.shape, dtype=np.float64)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                output[row, col] = self.naive_2D_filter_v2_kernel(img, row, col)
        return output

    @numba.jit(numba.float64(numba.object_, numba.float64[:]))
    def bilateral_kernel(self, input):
        centre = input[12]
        output = 0.0
        weight_sum = 0.0
        for idx in xrange(25):
            dr = input[idx] - centre
            range_contrib = range_coefficient * np.exp(-1.0 * dr * dr / range_divisor)
            total_weights = range_contrib * spatial_gaussian_weights[idx]
            output += input[idx] * total_weights
            weight_sum += total_weights
        return output / weight_sum

    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def naive_2D_filter_v4(self, img):
        output = np.zeros(img.shape, dtype=np.float64)
        window = np.zeros((25,), dtype=np.float64)
        for row in xrange(img.shape[0]):
            for col in xrange(img.shape[1]):
                idx = 0
                for r in xrange(row-2, row+3):
                    for c in xrange(col-2, col+3):
                        if (r >= 0) & (r < img.shape[0]) & (c >= 0) & (c < img.shape[1]):
                            window[idx] = img[r,c]
                        else:
                            window[idx] = 0.0
                        idx += 1
                output[row, col] = self.bilateral_kernel(window)
        return output
    
    @numba.jit(numba.float64[:,:](numba.object_, numba.float64[:,:]))
    def scipy_gen_filter(self, img):
        output = np.zeros(img.shape, dtype=img.dtype)
        ndfilt.generic_filter(img, self.bilateral_kernel, [5, 5], output=output, mode='constant', cval=0.0)
        return output

def opencv_test(test_image):
    return cv2.bilateralFilter(np.float32(test_image), 5, 20.0, 20.0)

def skimg_test(test_image):
    return skimg_bilateral(test_image, 5, 20.0, 20.0, mode='constant', cval=0.0)

def benchmark_function(fn, test_image, fn_name):
    '''Benchmarking function, prints the running time for the given filter.
    
    Arguments:
    fn -- The function to benchmark, must take exactly one argument, a 2D
        NumPy array.
    test_image -- 2D NumPy array storing the image, i.e. Lenna.
    fn_name -- Text description of the function for printing nice output.
    
    '''
    start_time = datetime.now()
    output = fn(test_image)
    duration = datetime.now() - start_time
    plt.figure()
    plt.imshow(output, cmap=plt.get_cmap("gray"))
    plt.show(block=False)
    print("{:s}: {:f} seconds.".format(fn_name, duration.total_seconds()))
    return output

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''
    
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by William John Shipman on %s.
  Copyright 2013 William John Shipman. All rights reserved.
  
  Licensed under the GNU General Public License version 3 or later
  http://www.gnu.org/licenses/
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        
        # Process arguments
        args = parser.parse_args()
        
        verbose = args.verbose
        
        if verbose > 0:
            print("Verbose mode on")
        
        # Stops Numba from printing LLVM IR to the console, as well as other debug messages.
        if DEBUG == 0:
            numba.logging.disable(numba.logging.DEBUG)

        bilateral_filters = BilateralFilters()
        bilateral_filters_numba = BilateralFiltersNumba()
        bilateral_filters_numba_v2 = BilateralFiltersNumba_v2()

        # Load Lenna as a gray-scale image and show it.
        test_image = np.float64(ndimage.imread("../Lenna.png", flatten=True))
        plt.figure()
        plt.imshow(test_image, cmap=plt.get_cmap("gray"))
        plt.show(block=False)
        
        benchmark_function(bilateral_filters.naive_2D_filter_v1, test_image, "naive_2D_filter_v1")
        benchmark_function(bilateral_filters_numba.naive_2D_filter_v1, test_image, "naive_2D_filter_v1 (Numba)")
        benchmark_function(bilateral_filters_numba.naive_2D_filter_v2, test_image, "naive_2D_filter_v2 (Numba)")
        benchmark_function(bilateral_filters.naive_2D_filter_v3, test_image, "naive_2D_filter_v3")
        benchmark_function(bilateral_filters.naive_2D_filter_v4, test_image, "naive_2D_filter_v4")
        benchmark_function(bilateral_filters_numba.naive_2D_filter_v4, test_image, "naive_2D_filter_v4 (Numba)")
              
        benchmark_function(bilateral_filters.scipy_gen_filter, test_image, "scipy_gen_filter")
        benchmark_function(bilateral_filters_numba.scipy_gen_filter, test_image, "scipy_gen_filter (Numba)")
        
        benchmark_function(bilateral_filters_numba_v2.naive_2D_filter_v1, test_image, "naive_2D_filter_v1 (Numba v2)")
        benchmark_function(bilateral_filters_numba_v2.naive_2D_filter_v2, test_image, "naive_2D_filter_v2 (Numba v2)")
        benchmark_function(bilateral_filters_numba_v2.naive_2D_filter_v4, test_image, "naive_2D_filter_v4 (Numba v2)")
        benchmark_function(bilateral_filters_numba_v2.scipy_gen_filter, test_image, "scipy_gen_filter (Numba v2)")
              
        benchmark_function(bilateral_filters.sklearn_2d_filter, test_image, "sklearn_2d_filter")
        benchmark_function(bilateral_filters_numba.sklearn_2d_filter, test_image, "sklearn_2d_filter (Numba)")
          
        benchmark_function(opencv_test, test_image, "OpenCV")
        output_img = benchmark_function(skimg_test, test_image, "scikit-image")
         
        if DEBUG:
            imsave("lenna_bilat_filtered.jpg", np.uint8(output_img))
        
        plt.show()
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    if DEBUG:
        sys.argv.append("-v")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'filter2dimage_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())