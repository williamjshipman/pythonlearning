#!/usr/local/bin/python2.7
# encoding: utf-8
'''
adaptive_histeq -- Adaptive Histogram Equalisation in OpenCL and OpenGL example

adaptive_histeq is a demonstration of combining PyOpenCL and PyOpenGL to
perform adaptive histogram equalisation.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details (http://www.gnu.org/licenses/).

@author:     William John Shipman
        
@copyright:  2013 William John Shipman. All rights reserved.
        
@license:    GNU General Public License version 3 (or later).

@contact:    user_email
@deffield    updated: 2013-11-02
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import pyopencl as cl

import scipy.ndimage as ndimage

import numpy as np

import matplotlib.pyplot as plt

__all__ = []
__version__ = 0.1
__date__ = '2013-11-02'
__updated__ = '2013-11-02'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

class AdaptiveHistEqApp:
    '''Application logic for adaptive histogram equalisation.'''
    def __init__(self, args, img_dims, ch_order):
        if args.verbose > 0:
            print('Initialising OpenCL objects.')
    
        self.platform = cl.get_platforms()[int(args.platform)]
        self.device = self.platform.get_devices()[int(args.device)]
    
        if args.verbose > 0:
            print('Creating OpenCL context')
        self.ctx = cl.Context(devices=(self.device,))
        
        self.mem_read_queue = cl.CommandQueue(self.ctx, device=self.device)
        self.mem_write_queue = cl.CommandQueue(self.ctx, device=self.device)
        self.exec_queue = cl.CommandQueue(self.ctx, device=self.device)
        
        if args.verbose > 0:
            print('Compiling kernel.')
        self.cl_program = cl.Program(self.ctx, '''
            kernel void adapt_hist_eq(
                    read_only image2d_t input,
                    write_only image2d_t output,
                    const sampler_t sampler,
                    int block_x, int block_y) {
                int x = get_global_id(0);
                int y = get_global_id(1);
                
                float4 pixel = read_imagef(input, sampler, (int2)(x,y));
                
                float4 min_px = (float4)(1.0f);
                int4 cdf = (int4)(0);
                int4 cdf_min = (int4)(0);
                
                //First pass to get the minimum intensity.
                for (int by = -block_y; by <= block_y; by++) {
                    for (int bx = -block_x; bx <= block_x; bx++) {
                        float4 block_px = read_imagef(input, sampler, (int2)(x+bx,y+by));
                        min_px = min(min_px, block_px);
                    }
                }
                
                //Second pass to get CDF(pixel) and CDF(min).
                for (int by = -block_y; by <= block_y; by++) {
                    for (int bx = -block_x; bx <= block_x; bx++) {
                        float4 block_px = read_imagef(input, sampler, (int2)(x+bx,y+by));
                        cdf -= (block_px <= pixel);
                        cdf_min -= (block_px == min_px);
                    }
                }
                
                float4 output_px = (convert_float4(cdf - cdf_min) / convert_float4((int4)((2*block_x+1) * (2*block_y+1)) - cdf_min));
                output_px.w = 1.0f;
                
                write_imagef(output, (int2)(x,y), output_px);
            }
        ''').build()
        
        self.sampler = cl.Sampler(self.ctx, False, cl.addressing_mode.CLAMP, cl.filter_mode.LINEAR)
        
        if args.verbose > 0:
            print('Creating input and output images in device memory.')
        self.input_image_cl = cl.Image(self.ctx, cl.mem_flags.READ_ONLY, cl.ImageFormat(ch_order, cl.channel_type.UNORM_INT8), img_dims)
        self.output_image_cl = cl.Image(self.ctx, cl.mem_flags.WRITE_ONLY, cl.ImageFormat(ch_order, cl.channel_type.UNORM_INT8), img_dims)
        
    def set_input_img(self, input_img):
        '''Copies the input image to the OpenCL image object.'''
#         cl.enqueue_copy(self.mem_write_queue, self.input_image_cl, input_img.reshape(self.input_image_cl.shape + (self.input_image_cl.format.channel_count,)), is_blocking=True, origin=(0, 0), region=input_img.shape[1::-1])
        cl.enqueue_copy(self.mem_write_queue, self.input_image_cl, input_img, is_blocking=True, origin=(0, 0), region=input_img.shape[1::-1])
    
    def get_output_img(self):
        output_img = np.zeros(self.output_image_cl.shape[1::-1] + (self.output_image_cl.format.channel_count,), dtype=np.uint8)
        cl.enqueue_copy(self.mem_read_queue, output_img, self.output_image_cl, is_blocking=True, origin=(0,0), region=self.output_image_cl.shape)
        return output_img
    
    def run(self):
        self.cl_program.adapt_hist_eq(self.exec_queue, self.input_image_cl.shape, None, self.input_image_cl, self.output_image_cl, self.sampler, np.int32(50), np.int32(50)).wait()

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''
    
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by William John Shipman on %s.
  Copyright 2013 William John Shipman. All rights reserved.
  
  Licensed under the GNU General Public License version 3 (or later)
  https://gnu.org/licenses/gpl.html
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.
  This is free software, and you are welcome to redistribute it
  under certain conditions, see the GNU GPL v3 or later.
  
USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-p', '--platform', metavar='platform', help='OpenCL platform number [default: %(default)s]', default='0')
        parser.add_argument('-d', '--device', metavar='device', help='OpenCL device number [default: %(default)s]', default='0')
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument(dest="path", help="path to input image [default: %(default)s]", metavar="path")
        
        # Process arguments
        args = parser.parse_args()
        
        img_path = args.path
        verbose = args.verbose
        
        if verbose > 0:
            print("Verbose mode on")
            print('Input image: {:s}'.format(img_path))

        #Read in the image.
        input_img = ndimage.imread(img_path)
        if verbose > 0:
            mode_strs = {1:'GRAY', 3:'RGB', 4:'RGBA'}
            print('Input image properties: {:d}x{:d} {:s} {:s}'.format(input_img.shape[1], input_img.shape[0], mode_strs[input_img.shape[2]], str(input_img.dtype)))
        
        if input_img.shape[2] == 3:
            if verbose > 0:
                print('Input image is in RGB format but OpenCL only supports RGB with a limited number of data formats, converting to RGBA.')
            input_img = np.append(input_img, 255 * np.ones((input_img.shape[0], input_img.shape[1], 1), dtype=input_img.dtype), 2);
        
        channel_order_dict = {1:cl.channel_order.LUMINANCE, 4:cl.channel_order.RGBA}
        app = AdaptiveHistEqApp(args, input_img.shape[1::-1], channel_order_dict[input_img.shape[2]])
        app.set_input_img(input_img)
        if verbose > 0:
            print('Running AHE.')
        app.run()
        if verbose > 0:
            print('Getting output image.')
        output_img = app.get_output_img()
        
        plt.figure()
        plt.imshow(input_img)
        plt.figure()
        plt.imshow(output_img)
        plt.show()
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    if DEBUG:
#         sys.argv.append("-h")
        sys.argv.append("-v")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'adaptive_histeq_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())