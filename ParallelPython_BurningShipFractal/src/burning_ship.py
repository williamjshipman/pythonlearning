#!/usr/local/bin/python2.7
# encoding: utf-8
'''
burning_ship -- Parallel Python Burning Ship fractal.

burning_ship is a Parallel Python implementation of the Burning Ship fractal

@author:     William John Shipman
        
@copyright:  2013 William John Shipman. All rights reserved.
        
@license:    GNU GPL v3 or later

@deffield    updated: 2013-12-14
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import pp #Import Parallel Python.

import numpy

import matplotlib.pyplot as plt

from numba import jit

import matplotlib.image as mpimg

from datetime import datetime

__all__ = []
__version__ = 0.1
__date__ = '2013-12-14'
__updated__ = '2014-12-25'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

@jit
def burning_ship_complex(cx, cy, maxiterations, fmaxiter):
    c = complex(cx, cy)
    p = complex(0.0, 0.0)
    k = 0
    while k < maxiterations:
        temp = complex(abs(p.real), abs(p.imag))
        p = temp * temp - c
        if ((p.real*p.real + p.imag*p.imag) > 10.0):
            break
        k+=1
    return 1.0 - k / fmaxiter

@jit
def burning_ship_scalar(cx, cy, maxiterations, fmaxiter):
    [px, py] = [0.0, 0.0]
    k = 0
    while k < maxiterations:
        [px, py] = [px*px - py*py - cx, 2 * abs(px*py) - cy]
        if ((px*px + py*py) > 10.0):
            break
        k+=1
    return 1.0 - k / fmaxiter

def burning_ship_default(cx, cy, maxiterations, fmaxiter):
    [px, py] = [0.0, 0.0]
    k = 0
    while k < maxiterations:
        [px, py] = [px*px - py*py - cx, 2 * abs(px*py) - cy]
        if ((px*px + py*py) > 10.0):
            break
        k+=1
    return 1.0 - k / fmaxiter

@jit
def burning_ship_scalar_v2(cx, cy, maxiterations, fmaxiter):
    p0x = 0.0
    p0y = 0.0
    k = 0
    while k < maxiterations:
        px = p0x*p0x - p0y*p0y - cx
        py = 2 * abs(p0x*p0y) - cy
        p0x = px
        p0y = py
        if ((px*px + py*py) > 10.0):
            break
        k+=1
    return 1.0 - k / fmaxiter

@jit
def create_fractal(block_min, block_max, block_px, maxiterations, img, op):
    xmin = block_min[1]
    ymin = block_min[0]
      
    dx = (block_max[1] - block_min[1]) / block_px[1]
    dy = (block_max[0] - block_min[0]) / block_px[0]
      
    fmaxiter = float(maxiterations)
      
    for y in xrange(block_px[0]):
        cy = ymin + y * dy
        for x in xrange(block_px[1]):
            cx = xmin + x * dx
            img[y,x] = op(cx, cy, maxiterations, fmaxiter)
#             img[y,x] = burning_ship_complex(cx, cy, maxiterations, fmaxiter)
#             img[y,x] = burning_ship_scalar(cx, cy, maxiterations, fmaxiter)
#             img[y,x] = burning_ship_scalar_v2(cx, cy, maxiterations, fmaxiter)
    return img

def burning_ship_fractal_worker_generator(fn):
    def burning_ship_fractal_worker_default(block_min, block_max, block_px, maxiterations, image):
        return burning_ship.create_fractal(block_min, block_max, block_px, maxiterations, image, burning_ship.burning_ship_default)
    def burning_ship_fractal_worker_scalar(block_min, block_max, block_px, maxiterations, image):
        return burning_ship.create_fractal(block_min, block_max, block_px, maxiterations, image, burning_ship.burning_ship_scalar)
    def burning_ship_fractal_worker_scalar_v2(block_min, block_max, block_px, maxiterations, image):
        return burning_ship.create_fractal(block_min, block_max, block_px, maxiterations, image, burning_ship.burning_ship_scalar_v2)
    def burning_ship_fractal_worker_complex(block_min, block_max, block_px, maxiterations, image):
        return burning_ship.create_fractal(block_min, block_max, block_px, maxiterations, image, burning_ship.burning_ship_complex)
    if fn == 'default':
        return burning_ship_fractal_worker_default
    elif fn == 'scalar':
        return burning_ship_fractal_worker_scalar
    elif fn == 'scalar_v2':
        return burning_ship_fractal_worker_scalar_v2
    elif fn == 'complex':
        return burning_ship_fractal_worker_complex
    else:
        raise RuntimeError('Unrecognised fractal generator {:s}!'.format(fn))

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''
    
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by William John Shipman on %s.
  Copyright 2013 William John Shipman. All rights reserved.
  
  Licensed under the GNU General Public License version 3 or later
  http://www.gnu.org/licenses/
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")
        parser.add_argument("-o", "--output", dest="output", help="output fractal image to this path. [default: %(default)s]", metavar="path", default="./burning_ship_pp.png")
        parser.add_argument("--xmin", dest="xminimum", help="minimum value for X [default: %(default)s]", default="-1.25")
        parser.add_argument("--ymin", dest="yminimum", help="minimum value for Y [default: %(default)s]", default="-1.2")
        parser.add_argument("--xmax", dest="xmaximum", help="maximum value for X [default: %(default)s]", default="2.15")
        parser.add_argument("--ymax", dest="ymaximum", help="maximum value for Y [default: %(default)s]", default="2.2")
        parser.add_argument("--dim", dest="grid_dimension", help="number of blocks along the x and y axes of the image [default: %(default)s]", default="2")
        parser.add_argument("--block", dest="block_dimension", help="width and height of the image block generated by a worker process [default: %(default)s]", default=256)
        parser.add_argument("--iter", dest="max_iterations", help="maximum number of iterations before declaring pixel convergence [default: %(default)s]", default=256)
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        
        # Process arguments
        args = parser.parse_args()
        
        verbose = args.verbose
        output_path = args.output
        xmin = numpy.float64(args.xminimum)
        ymin = numpy.float64(args.yminimum)
        xmax = numpy.float64(args.xmaximum)
        ymax = numpy.float64(args.ymaximum)
        grid_dimensions = numpy.int32(args.grid_dimension)
        block_dimension = numpy.int32(args.block_dimension)
        max_iterations = numpy.int32(args.max_iterations)
        
        if verbose > 0:
            print("Verbose mode on")
        
        # Start the PP job server on the local machine.
        if verbose > 0:
            print("Starting Parallel Python job server...")
        pp_server = pp.Server()
        
        # The first function, "scalar", is tested twice as the first test is dominated by the time Numba takes to JIT all my functions.
        for burning_ship_fn in ['default', 'default', 'scalar', 'scalar', 'scalar_v2', 'scalar_v2', 'complex', 'complex']:
            if verbose > 0:
                print("Submitting jobs to the PP server...")
            
            block_delta_x = numpy.float64(xmax - xmin) / numpy.float64(grid_dimensions)
            block_delta_y = numpy.float64(ymax - ymin) / numpy.float64(grid_dimensions)
            
            jobs = []
    
            start_time = datetime.now()
            for by in xrange(grid_dimensions):
                block_ymin = block_delta_y * by + ymin
                block_ymax = block_delta_y * (by+1) + ymin
                for bx in xrange(grid_dimensions):
                    block_xmin = block_delta_x * bx + xmin
                    block_xmax = block_delta_x * (bx+1) + xmin
                    jobs.append(
                                (by, bx,
                                 pp_server.submit(
                                                  burning_ship_fractal_worker_generator(burning_ship_fn),
                                                  (
                                                   [block_ymin, block_xmin], [block_ymax, block_xmax],
                                                   [block_dimension, block_dimension], max_iterations,
                                                   numpy.zeros((block_dimension, block_dimension))
                                                   ),
                                                  (),
                                                  ("numpy", "burning_ship"),
                                                  globals=globals())))
              
            if verbose > 0:
                print("Getting job results from the PP server...")
            output = numpy.zeros((block_dimension*grid_dimensions, block_dimension*grid_dimensions))
            for job in jobs:
                by = job[0]
                bx = job[1]
                job_output = job[2]()
                output[(by*block_dimension):((by+1)*block_dimension), (bx*block_dimension):((bx+1)*block_dimension)] = job_output
            time_delta = datetime.now() - start_time
    
    #         output = burning_ship_fractal_worker([ymin, xmin], [ymax, xmax], [block_dimension, block_dimension], max_iterations, numpy.zeros((block_dimension, block_dimension)))
            
            print('Running time for "{:s}" {:}x{:} grid of {:}x{:} pixel blocks: {:f}s'.format(burning_ship_fn, grid_dimensions, grid_dimensions, block_dimension, block_dimension, time_delta.total_seconds()))
        
        if verbose > 0:
            print("Saving output images...")
        vmin=output.min()
        vmax=output.max()
        mpimg.imsave("output_{:}x{:}_{:}x{:}_gray.png".format(grid_dimensions, grid_dimensions, block_dimension, block_dimension), output, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('gray'), origin="lower")
        mpimg.imsave("output_{:}x{:}_{:}x{:}_jet.png".format(grid_dimensions, grid_dimensions, block_dimension, block_dimension), output, vmin=vmin, vmax=vmax, cmap=plt.get_cmap('jet'), origin="lower")
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
#     except Exception, e:
#         if DEBUG or TESTRUN:
#             raise(e)
#         indent = len(program_name) * " "
#         sys.stderr.write(program_name + ": " + repr(e) + "\n")
#         sys.stderr.write(indent + "  for help use --help")
#         return 2

if __name__ == "__main__":
    if DEBUG:
        sys.argv.append("-v")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'burning_ship_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())